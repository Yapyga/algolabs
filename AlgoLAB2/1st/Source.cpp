#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <Windows.h>
#include <time.h>
#include <math.h>
#include <locale.h>
#define X 1000

void genre(int xfirst, int arr[X]) {
	unsigned long int  c = 1, a = 69069, m = 4294967294;

	unsigned long int tmp = xfirst;
	arr[0] = tmp % 300;
	for (int i = 0; i <= X - 1; i++) {
		tmp = (a*tmp + c) % m;
		arr[i + 1] = tmp % 300;
		printf("\nx[%d]=%d\n", i, arr[i]);
	}

}
void frequency(int arr[X], int freq[300]) {
	printf("");
	int tmp = 0, k;
	for (int i = 0; i < 300; i++) {
		k = 0;
		for (int j = 0; j < X; j++) {
			if (tmp == arr[j]) {
				k++;
			}
		}
		printf("\n����� %d ����������� %d ����.\n", tmp, k);
		freq[i] = k;
		tmp++;
	}
}

void possibility(int arr[X], int freq[300], double possibl[300])
{
	for (int i = 0; i < 300; i++)
	{
		possibl[i] = (double)freq[i] / X;
		printf("\n��������� ����� ����� %d - %.3lf\n", i, possibl[i]);
	}
}

double mathExpectation(int arr[X], double possibl[300])
{
	double mathExp = 0;
	for (int i = 0; i < 300; i++)
		mathExp += i * possibl[i];
	return mathExp;
}

double dispersion(int arr[X], double possibl[300], double mathExp)
{
	double disp = 0;
	for (int i = 0; i < 100; i++)
		disp += pow((i - mathExp), 2) * possibl[i];
	return disp;
}

int main() 
{
	setlocale(LC_ALL, "Russian");
	int xfirst;
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
  	xfirst = time(0);
	int arr[X], freq[300];
	double possibl[300];
	genre(xfirst, arr);
	frequency(arr, freq);
	possibility(arr, freq, possibl);
	double mathExp = mathExpectation(arr, possibl), disp = dispersion(arr, possibl, mathExp);
	printf("\n\n����������� ���������: %.3lf\n\n", mathExp);
	printf("��������: %lf", disp);
	double deviation = sqrt(disp);
	printf("\n\n������������������� ���������: %lf\n", deviation);
	system("pause");
	return 0;
}