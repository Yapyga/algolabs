#include <iostream>
#include <stdio.h>
#include <Windows.h>

typedef int et;
struct Node
{
	et value;
	struct Node* next;
	struct Node* previous;
};
struct Stack
{
	struct Node* head;
	struct Node* tail;
	size_t size;
};
Stack* createStack()
{
	Stack* stack = (Stack*)malloc(sizeof(Stack));
	if (stack)
	{
		stack->size = 0;
		stack->head = stack->tail = NULL;
	}
	return stack;
}
bool isStackEmpty(Stack* stack)
{
	return ((stack->head == NULL) || (stack->tail == NULL));
}
int add(Stack* stack, et* data)
{
	Node* newNode = (Node*)malloc(sizeof(Node));
	newNode->value = *data;
	newNode->previous = NULL;
	if (isStackEmpty(stack))
	{
		stack->head = newNode;
		stack->tail = newNode;
	}
	else
	{
		newNode->previous = stack->tail;
		stack->tail = newNode;
	}
	stack->size++;
	return 0;
}
int pop(Stack* stack)
{
	Node* popItem = (Node*)malloc(sizeof(Node));
	if (stack->tail == NULL)
		return -1;
	popItem = stack->tail;
	stack->tail = popItem->previous;
	free(popItem);
	stack->size--;
	return 0;
}
int printStack(Stack* stack)
{
	if (isStackEmpty(stack))
	{
		printf("\n���� ������!\n");
		return -1;
	}
	Node* node = (Node*)malloc(sizeof(Node));
	node = stack->tail;
	printf("\n����: { ");
	for (int i = 0; i < stack->size; i++)
	{
		printf("%d ", node->value);
		node = node->previous;
	}
	printf("}\n");
	return 0;
}
Stack* clearStack(Stack* stack)
{
	Node* itemToRemove = stack->tail;
	Node* nextItem;
	while (itemToRemove != NULL)
	{
		nextItem = itemToRemove->previous;
		free(itemToRemove);
		if (itemToRemove != NULL)
			itemToRemove = nextItem;
	}
	free(stack);
	return NULL;
}


int main()
{
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	int x;
	Stack* stack = NULL;
	et elemvalue = NULL;
	printf("1. �������� ����\n2. ������ ������� �� �����\n3. �������� ������� � �����\n4. ������� �������� ����� �� �������\n5. ������� ����");
	do {
		printf("\n��� ����: ");
		scanf_s("%d", &x);
		switch (x)
		{
		case 1:
			stack = createStack();
			break;
		case 2:
		{
			printf("\n������ �����, �� ������� ������: ");
			scanf_s("%d", &elemvalue);
			add(stack, &elemvalue);
		}break;
		case 3:
		{
			pop(stack);
		}break;
		case 4:
			printStack(stack);
			break;
		case 5:
		{
			if (clearStack(stack) == NULL)
				printf("\n���� ���������\n");
			else
				printf("\n������� ��������� �����!\n");
		}break;
		default:
			printf("\n�� ���������� ��� �����i!\n");
			return 0;
		}
	} while (true);
	return 0;
}
