#include <iostream>
#include <stdio.h>
#include <Windows.h>

typedef int et;
struct Node
{
	et value;
	struct Node* next;
	struct Node* previous;
};
struct Queue
{
	struct Node* head;
	struct Node* tail;
	size_t size;
};

Queue* createQueue()
{
	Queue* queue = (Queue*)malloc(sizeof(Queue));
	if (queue)
	{
		queue->size = 0;
		queue->head = queue->tail = NULL;
	}
	return queue;
}
bool isQueueEmpty(Queue* queue)
{
	return ((queue->head == NULL) || (queue->tail == NULL));
}
int add(Queue * queue, et * data)
{
	Node* newNode = (Node*)malloc(sizeof(Node));
	newNode->value = *data;
	newNode->next = NULL;
	if (isQueueEmpty(queue))
	{
		queue->head = newNode;
		queue->tail = newNode;
	}
	else
	{
		queue->tail->next = newNode;
		queue->tail = newNode;
	}
	queue->size++;
	return 0;
}
int pop(Queue* queue)
{
	Node* popItem = (Node*)malloc(sizeof(Node));
	if (queue->head == NULL)
		return -1;
	popItem = queue->head;
	queue->head = popItem->next;
	popItem->next = NULL;
	popItem->value = NULL;
	free(popItem);
	queue->size--;
	return 0;
}
int printQueue(Queue * queue)
{
	if (isQueueEmpty(queue))
	{
		printf("\n����� �����!\n");
		return -1;
	}
	Node* node = (Node*)malloc(sizeof(Node));
	node = queue->head;
	printf("\n�����: { ");
	for (int i = 0; i < queue->size; i++)
	{
		printf("%d ", node->value);
		node = node->next;
	}
	printf("}\n");
	return 0;
}
Queue* clearQueue(Queue* queue)
{
	Node* itemToRemove = queue->head;
	Node* nextItem;
	while (itemToRemove != NULL)
	{
		nextItem = itemToRemove->next;
		free(itemToRemove);
		if (itemToRemove != NULL)
			itemToRemove = nextItem;
	}
	free(queue);
	return NULL;
}
int main()
{
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	int x;
	Queue* queue = NULL;
	et elemvalue = NULL;
	printf("1. �������� �����\n2. ������ ������� � ����� \n3. �������� ������� �� �����\n4. ������� �������� ����� �� �������\n5. ������� �����");
	do {
		printf("\n�������: ");
		scanf_s("%d", &x);
		switch (x)
		{
		case 1:
			queue = createQueue();
			break;
		case 2:
		{
			printf("\n������ �����, �� ������� ������: ");
			scanf_s("%d", &elemvalue);
			add(queue, &elemvalue);
		}break;
		case 3:
		{
			pop(queue);
		}break;
		case 4:
			printQueue(queue);
			break;
		case 5:
		{
			if (clearQueue(queue) == NULL)
				printf("\n����� ���� ��������!\n");
			else
				printf("\n������� ��� ��������� �����!\n");
		}break;
		default:
			printf("\n������� ������!\n");
			return 0;
		}
	} while (true);
	return 0;
}