#include <iostream>
#include <stdio.h>
#include <Windows.h>

typedef int et;

struct Node
{
	et value;
	struct Node* next;
	struct Node* previous;
};

struct cList
{
	struct  Node* head;
	struct Node* tail;
	size_t size;
};

cList* createList()
{
	cList* list = (cList*)malloc(sizeof(cList));
	if (list)
	{
		list->size = 0;
		list->head = list->tail = NULL;
	}
	return list;
}
bool isListEmpty(cList* list)
{
	return ((list->head == NULL) || (list->tail == NULL));
}
int add(cList * list, et * data, int index)
{
	Node* newNode = (Node*)malloc(sizeof(Node));
	newNode->value = *data;
	if (index == 0)
	{
		if (isListEmpty(list))
		{
			newNode->next = NULL;
			newNode->previous = NULL;
			list->head = newNode;
			list->tail = newNode;
		}
		else
		{
			newNode->previous = NULL;
			list->head->previous = newNode;
			newNode->next = list->head;
			list->head = newNode;
		}
		list->size++;
	}
	else
	{
		if (isListEmpty(list))
			return -2;
		if (index != list->size)
		{
			if (index >= list->size || index <= 0)
				return -2;
			Node * oldNode = (Node*)malloc(sizeof(Node));
			if (index < list->size / 2)
			{
				oldNode = list->head;
				for (int i = 1; i < index - 1; i++)
				{
					oldNode = oldNode->next;
				}
				newNode->next = oldNode->next;
				newNode->previous = oldNode;
				oldNode->next = newNode;
			}
			else
			{
				oldNode = list->tail;
				for (int i = list->size; i > index; i--)
				{
					oldNode = oldNode->previous;
				}
				newNode->previous = oldNode;
				newNode->next = oldNode->next;
				oldNode->next = newNode;
			}
			list->size++;
			return 0;
		}
		else
		{
			Node* oldNode = (Node*)malloc(sizeof(Node));
			oldNode = list->tail;
			oldNode->next = newNode;
			newNode->previous = oldNode;
			list->tail = newNode;
			list->tail->next = NULL;
			list->size++;
		}
	}
}
int remove(cList* list, int ind)
{
	Node* temp = (Node*)malloc(sizeof(Node));
	Node* nodeToRemove = (Node*)malloc(sizeof(Node));
	if (isListEmpty(list))
		return -1;
	if (ind == 0)
	{
		nodeToRemove = list->head;
		list->head = list->head->next;
		list->head->previous = NULL;
		nodeToRemove->next = NULL;
		nodeToRemove->value = NULL;
		free(nodeToRemove);
		list->size--;
		return 0;
	}
	else
	{
		if (ind != list->size - 1)
		{
			if (ind < list->size / 2)
			{
				temp = list->head;
				for (int i = 1; i < ind - 1; i++)
				{
					temp = temp->next;
				}
				nodeToRemove = temp->next;
				temp->next = nodeToRemove->next;
				nodeToRemove->next->previous = temp;
				nodeToRemove->previous = NULL;
				nodeToRemove->next = NULL;
				nodeToRemove->value = NULL;
				free(nodeToRemove);
			}
			else
			{
				temp = list->tail;
				for (int i = list->size; i > ind + 1; i--)
				{
					temp = temp->previous;
				}
				nodeToRemove = temp->previous;
				temp->previous = nodeToRemove->previous;
				nodeToRemove->previous->next = temp;
				nodeToRemove->previous = NULL;
				nodeToRemove->next = NULL;
				nodeToRemove->value = NULL;
				free(nodeToRemove);
			}
			list->size--;
			return 0;
		}
		else
		{
			nodeToRemove = list->tail;
			list->tail = nodeToRemove->previous;
			list->tail->next = NULL;
			nodeToRemove->previous = NULL;
			nodeToRemove->next = NULL;
			nodeToRemove->value = NULL;
			free(nodeToRemove);
			list->size--;
		}
	}
}
int printList(cList* list)
{
	if (isListEmpty(list))
		return -1;
	Node* node = (Node*)malloc(sizeof(Node));
	node = list->head;
	printf("������: | ");
	for (int i = 0; i < list->size; i++)
	{
		printf("%d ", node->value);
		node = node->next;
	}
	printf("|\n");
	return 0;
}
cList* clearList(cList* list)
{
	Node* itemToRemove = list->head;
	Node* nextItem;
	while (itemToRemove != NULL)
	{
		nextItem = itemToRemove->next;
		free(itemToRemove);
		if (itemToRemove != NULL)
			itemToRemove = nextItem;
	}
	free(list);
	list->head = NULL;
	list->size = NULL;
	list->tail = NULL;
	list = NULL;
	return list;
}


int main()
{
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	int x;
	cList* list = NULL;
	et elemvalue = NULL;
	printf("1. �������� ������\n2. ������ �������\n3. �������� �������\n4. ������� ������ �� �������\n5. ������� ������");
	do {
		printf("\n��� ����: ");
		scanf_s("%d", &x);
		switch (x)
		{
		case 1:
			list = createList();
			break;
		case 2:
		{
			printf("\n������� ������ �������� (�� 0 �� %d): ", list->size);
			scanf_s("%d", &x);
			printf("\n������ ��������: ");
			scanf_s("%d", &elemvalue);
			add(list, &elemvalue, x);
		}break;
		case 3:
		{
			printf("\n������� ������� �������� ��� ���������(�� 0 �� %d): ",
				list->size - 1);
			scanf_s("%d", &x);
			remove(list, x);
		}break;
		case 4:
			printList(list);
			break;
		case 5:
		{
			if (clearList(list) == NULL)
				printf("\n������ ���������.\n");
			else
				printf("\n������� ��������� ������!\n");
		}break;
		default:
			printf("\n������� ��� �����i!\n");
			return 0;
		}
	} while (true);
	return 0;
}