﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp
{
    class Program
    {

        static float check()
        {
            while (true)
            {
                if (float.TryParse(Console.ReadLine(), out float x))
                    return x;
                else
                    Console.WriteLine("Помилка!");
            }
        }

        static float f(float x)
        {
            return 3 * x * x * x + x * x - 5;
        }

        static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.Unicode;
            Console.InputEncoding = Encoding.Unicode;
            float a = 0, b = 0, n = 0, h, x;
            Console.Write("Введіть a: ");
            a = check();
            Console.Write("Введіть b: ");
            b = check();
            Console.Write("Введіть кількість точок: ");
            n = check();
            Console.Write("Результати:\n");
            h = (b - a) / n;
            float sRect = 0;
            for (int i = 1; i <= n; i++)
            {
                x = a + i * h;
                sRect += f(x) * h;
            }
            Console.WriteLine($"1. Метод прямокутників: {sRect}");
            float sTrap = 0, a2 = a;
            for (int i = 1; i <= n; i++)
            {
                x = (2 * a2 + h) / 2;
                sTrap += f(x) * h;
                a2 += h;
            }
            Console.WriteLine($"2. Метод трапеції: {sTrap}");
            float sSimp = 0;
            sSimp = ((b - a) / 6) * (f(a) + 4 * f((a + b) / 2) + f(b));
            Console.WriteLine($"3. Метод Сімсона: {sSimp}");
        }
    }
}
