#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <time.h>
#include <windows.h>

union DataStructure
{
	struct
	{
		signed short n1 : 1;
		signed short n2 : 1;
		signed short n3 : 1;
		signed short n4 : 1;
		signed short n5 : 1;
		signed short n6 : 1;
		signed short n7 : 1;
		signed short n8 : 1;
		signed short n9 : 1;
		signed short n10 : 1;
		signed short n11 : 1;
		signed short n12 : 1;
		signed short n13 : 1;
		signed short n14 : 1;
		signed short n15 : 1;
		signed short n16 : 1;
	}type;
	signed short dataStruture;
};



int main()
{
	DataStructure d;
	signed short b;
	printf("Structure definition\nEnter the number: ");
	scanf("%hi", &b);
	d.dataStruture = b;
	if ((d.type.n16 & 1) == 0)
		printf("Positive number\nValue = %hi", b);
	else printf("Negative number\nValue = %hi", b);


	signed short a;
	printf("\n\nArithmetic\nEnter the number: ");
	scanf("%hi", &a);
	if (a < 0)
		printf("Negative number\nValue = %hi\n", -a);
	else if (a > 0)
		printf("Positive number\nValue = %hi\n", a);

	printf("\n\nLogical\nEnter the number: ");
	scanf("%hi", &a);
	bool check = (a < 0);
	if (check)  printf("Negative number\nValue = %hi", -a);
	else printf("Positive number\nValue = %hi\n", a);
	system("pause");
	return 0;
}