#include <stdio.h>
#include <windows.h>

union Data
{
	struct
	{
		unsigned char low;
		unsigned char low2;
		unsigned char high;
		unsigned char high2;
	}byte;
	float data;
};
void binaryCode(unsigned char n)
{
	unsigned char number[8];
	for (int i = 0; i < 8; i++)
	{
		number[i] = n % 2;
		n = n / 2; 
	}
	for (int i = 7; i >= 0; i--)
		printf("%d", number[i]); 
}
void binaryForward(unsigned char n, int number1st, int number2nd) 
{
	unsigned char number[8];
	for (int i = 0; i < 8; i++)
	{
		number[i] = n % 2;
		n = n / 2;
	}
	for (int i = number1st; i <= number2nd; i++)
		printf("%d", number[i]);
}
void binaryBack(unsigned char n, int number1st, int number2nd)

{
	unsigned char number[8];
	for (int i = 0; i < 8; i++)
	{
		number[i] = n % 2;
		n = n / 2;
	}
	for (int i = number1st; i >= number2nd; i--)
		printf("%d", number[i]);
}

int main()
{
	Data pointer;
	int sign = 7;
	printf("Enter your value: "); scanf_s("%f", &pointer.data);
	printf("~~~~~~~~~~~~~ Memory: %lu ~~~~~~~~~~~~~\n", sizeof(Data));
	printf("Bit values: \n");
	binaryCode(pointer.byte.high2);
	printf(" ");
	binaryCode(pointer.byte.high);
	printf(" ");
	binaryCode(pointer.byte.low2);
	printf(" ");
	binaryCode(pointer.byte.low);
	printf("\n");
	printf("Low: %d ", pointer.byte.low);
	printf("Lower: %d ", pointer.byte.low2);
	printf("High: %d ", pointer.byte.high);
	printf("Higher: %d\n", pointer.byte.high2);
	printf("Sign: ");
	binaryForward(pointer.byte.high2, sign, sign);
	printf("\nCharacteristic: ");
	binaryBack(pointer.byte.high2, 6, 0);
	binaryForward(pointer.byte.high, sign, sign);
	printf("\nManstis: ");
	binaryBack(pointer.byte.high, 6, 0); printf(" ");
	binaryCode(pointer.byte.low2); printf(" ");
	binaryCode(pointer.byte.low);
	printf("\n");

	system("Pause");
	return 0;

}