#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <time.h>
#include <windows.h>

struct my_local_time
{
	unsigned short year : 7;
	unsigned short month : 4;
	unsigned short day : 5;
	unsigned short hour : 5;
	unsigned short minute : 6;
	unsigned short second : 6;
};

int main()
{
	struct my_local_time tmp;
	struct tm *ptr, x;
	time_t It;
	It = time(NULL);
	ptr = localtime(&It);
	x.tm_hour = ptr->tm_hour; x.tm_isdst = ptr->tm_isdst; x.tm_mday = ptr->tm_mday; x.tm_min = ptr->tm_min; x.tm_mon = ptr->tm_mon; x.tm_sec = ptr->tm_sec; x.tm_wday = ptr->tm_wday; x.tm_yday = ptr->tm_yday; x.tm_year = ptr->tm_year;
	tmp.year = x.tm_year; tmp.month = x.tm_mon; tmp.day = x.tm_mday; tmp.hour = x.tm_hour; tmp.minute = x.tm_min; tmp.second = x.tm_sec;
	printf("Local time(function): %s\nLocal time(created): %.2d/%.2d/%d %.2d:%.2d:%.2d", asctime(ptr), tmp.day, tmp.month + 1, tmp.year + 1900, tmp.hour, tmp.minute, tmp.second);
	printf("\n\ntime.h structure: %d bytes\n\nMy structure: %d bytes\n", sizeof(x), sizeof(tmp));
	system("pause");
	return 0;
}