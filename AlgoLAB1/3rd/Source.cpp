#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <time.h>
#include <windows.h>

int main()
{
	signed char n1;
	n1 = 5 + 127; // 0000 0101 + 0111 1111 = 1000 0100 (0111 1100 � ������ ����)
	printf("5 + 127 = %d\n\n", n1);

	signed char n2;
	n2 = 2 - 3; // 0000 0010 � 0000 0011 = 1111 1111 (0000 0001 � ������ ����)
	printf("2 - 3 = %d\n\n", n2);

	signed char n3;
	n3 = -120 - 34; // 1000 1000 � 0010 0010 = 0110 0110
	printf("-120 - 34 = %d\n\n", n3);

	signed char n4;
	n4 = (unsigned char)(-5); // ������������ � 1111 1011 � 0000 0101
	printf("(unsigned char)(-5) = %d\n\n", n4);

	signed char n5;
	n5 = 56 & 38; // 0011 1000 & 0010 0110 = 0010 0000
	printf("56 & 38 = %d\n\n", n5);
	
	signed char n6;
	n6 = 56 | 38; // 0011 1000 | 0010 0110 = 0011 1110
	printf("56 | 38 = %d\n", n6);
	
	system("pause");
	return 0;
}